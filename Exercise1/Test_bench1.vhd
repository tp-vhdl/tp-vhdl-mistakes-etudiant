library IEEE;
use ieee.std_logic_1164.all;

entity tb_OUEX is end;

architecture ARCHI of tb_OUEX is
    signal A, B, S1, S2, S3 : std_logic;

begin

    DUT1 : entity work.OUEX(LOGIC_EQ)
        port map (
            I_A => A,
            I_B => B,
            O_S => S1
            );

    DUT2 : entity work.OUEX(DIRECT_TRUTH_TABLE)
        port map (
            I_A => A,
            I_B => B,
            O_S => S2
            );

    DUT3 : entity work.OUEX(PROCESS_TRUTH_TABLE)
        port map (
            I_A => A,
            I_B => B,
            O_S => S2
            );

    A <= '0', '1' after 10 ns, '0' after 20 ns, '1' after 30 ns;
    B <= '0', '1' after 20 ns;

end ARCHI;
