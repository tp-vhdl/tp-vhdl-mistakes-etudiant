
library IEEE;
use ieee.std_logic_1164.all;

entity tb_Counter2 is end;

architecture ARCHI of tb_Counter2 is

    signal CK, R, DETECT : std_logic := '0';

begin
    DUT : entity work.Counter2(Behavioral)
        generic map(
            N => 10
            )
        port map (
            I_Clock  => CK,
            I_Reset  => R,
            O_Detect => DETECT
            );

    CK <= not CK   after 10 ns;
    R  <= '0', '1' after 5 ns;

end ARCHI;
