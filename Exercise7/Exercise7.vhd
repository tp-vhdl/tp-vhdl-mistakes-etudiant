library IEEE;
use ieee.std_logic_1164.all;

entity Counter2 is
    generic (
        N : integer := 10
        );
    port (
        I_Clock  : in  std_logic;
        I_Reset  : in  std_logic;
        O_Detect : out std_logic
        );
end entity Counter2;


architecture Behavioral of Counter2 is

    signal S_Counter : integer;              -- counting signal

begin
    process (I_Clock, I_Reset)
    begin
        if (I_Reset = '0') then              -- initialization
            S_Counter <= 0;
        elsif rising_edge(I_Clock) then      -- or “if I_Clock’EVENT and  I_Clock =’1’” clock rising edge
            if (S_Counter = N-1) then
                S_Counter <= 0;              -- reset counter value at 0
            else
                S_Counter <= S_Counter + 1;  -- increment counting signal
            end if;
        end if;
    end process;

    O_Detect <= '1' when (S_Counter = N-1) else '0';

end Behavioral;
