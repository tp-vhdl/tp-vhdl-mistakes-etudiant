
library IEEE;
use ieee.std_logic_1164.all;

entity tb_Counter is end;

architecture ARCHI of tb_Counter is
    component Counter1 is
        generic (
            N : integer := 10
            );
        port (
            I_Clock : in  std_logic;
            I_Reset : in  std_logic;
            O_Count : out std_logic_vector (4 downto 0)
            );
    end component;

    signal CK, R : std_logic                    := '0';
    signal COUNT : std_logic_vector(4 downto 0) := "00000";

begin

    DUT : Counter1
        generic map (
            N => 12
            )
        port map (
            I_Clock => CK,
            I_Reset => R,
            O_Count => COUNT
            );

    CK <= not CK   after 10 ns;
    R  <= '0', '1' after 5 ns;

end ARCHI;
