library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Counter1 is
    generic (
        N : integer := 10
        );
    port (
        I_Clock : in  std_logic;
        I_Reset : in  std_logic;
        O_Count : out std_logic_vector (4 downto 0)
        );
end entity Counter1;


architecture Behavioral of Counter1 is

    signal S_Counter : integer range 0 to N-1;  -- counting signal

begin
    process (I_Clock, I_Reset)
    begin
        if (I_Reset = '0') then                 -- initialization
            S_Counter <= 0;
            O_Count   <= (others => '0');
        elsif rising_edge(I_Clock) then         -- or “if I_Clock’EVENT and  I_Clock =’1’” clock rising edge
            if (S_Counter = 0) then
                S_Counter <= N-1;               -- set counter value to N-1
            else
                S_Counter <= S_Counter -1;      -- increment counting signal
            end if;
            O_Count <= std_logic_vector(to_unsigned (S_Counter, 5));
        end if;
    end process;

end Behavioral;
