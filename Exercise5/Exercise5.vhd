library IEEE;
use ieee.std_logic_1164.all;

entity Operator is
    port (
        I_0   : in  std_logic;
        I_1   : in  std_logic;
        I_2   : in  std_logic;
        O_out : out std_logic_vector (7 downto 0)
        );
end Operator;

architecture Behavioral of Operator is
begin
    process (I_0, I_1, I_2)
    begin
        if I_2 = '0' and I_1 = '0' and I_0 = '0' then
            O_out <= "00000001";
        elsif I_2 = '0' and I_1 = '0' and I_0 = '1' then
            O_out <= "00000010";
        elsif I_2 = '0' and I_1 = '1' and I_0 = '0' then
            O_out <= "00000100";
        elsif I_2 = '0' and I_1 = '1' and I_0 = '1' then
            O_out <= "00001000";
        elsif I_2 = '1' and I_1 = '0' and I_0 = '0' then
            O_out <= "00010000";
        elsif I_2 = '1' and I_1 = '0' and I_0 = '1' then
            O_out <= "00100000";
        elsif I_2 = '1' and I_1 = '1' and I_0 = '0' then
            O_out <= "01000000";
        end if;
    end process;

end Behavioral;
