
library IEEE;
use ieee.std_logic_1164.all;

entity tb_Operator is end;

architecture ARCHI of tb_Operator is

    signal I0, I1, I2 : std_logic;
    signal IN_VECT    : std_logic_vector(2 downto 0);
    signal DEC_OUT    : std_logic_vector(7 downto 0);

begin
    DUT : entity work.Operator(Behavioral)
        port map (
            I_0   => I0,
            I_1   => I1,
            I_2   => I2,
            O_out => DEC_OUT
            );

    I0 <= IN_VECT(0);
    I1 <= IN_VECT(1);
    I2 <= IN_VECT(2);

    IN_VECT <= "101", "110" after 10 ns, "000" after 20 ns, "010" after 30 ns,
               "100" after 40 ns, "010" after 50 ns, "001" after 60 ns, "011" after 70 ns,
               "101" after 80 ns, "100" after 90 ns, "110" after 100 ns;
end ARCHI;
