library IEEE;
use ieee.std_logic_1164.all;

entity D_flip_flop is
    port (I_Clk   : in  std_logic;
          I_Reset : in  std_logic;
          I_D     : in  std_logic;
          O_Q     : out std_logic
          );
end D_flip_flop;

architecture Behavioral of D_flip_flop is
begin

    Init : process (I_Reset)
    begin
        if I_Reset = '1' then
            O_Q <= '0';
        end if;
    end process Init;

    Sync : process (I_Clk)
    begin
        if rising_edge(I_Clk) and I_Reset = '0' then
            O_Q <= I_D;
        end if;
    end process Sync;

end Behavioral;
