
library IEEE;
use ieee.std_logic_1164.all;

entity tb_D_flip_flop is end;

architecture ARCHI of tb_D_flip_flop is

    signal CK      : std_logic := '0';
    signal R, D, Q : std_logic;

begin
    DUT : entity work.D_flip_flop(Behavioral)
        port map (
            I_Clk   => CK,
            I_Reset => R,
            I_D     => D,
            O_Q     => Q
            );

    CK <= not CK   after 10 ns;
    R  <= '0', '1' after 2 ns, '0' after 15 ns;
    D  <= '1', '0' after 35 ns, '1' after 55 ns;

end ARCHI;
