----------------------------------------------
-- Testbench for ADDER1 and ADDER2
----------------------------------------------
entity tb_ADDER is end;

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

architecture ARCHI1 of tb_ADDER is
    signal INT_A, INT_B, INT_S    : integer;
    signal SIGN_A, SIGN_B, SIGN_S : signed(7 downto 0);
begin

    DUT1 : entity work.ADDER1(Behavioral)
        port map (
            A => INT_A,
            B => INT_B,
            S => INT_S
            );

    DUT2 : entity work.ADDER2(Behavioral)
        port map (
            A => SIGN_A,
            B => SIGN_B,
            S => SIGN_S
            );

    INT_A <= 0, -5   after 10 ns, 22 after 20 ns, -8 after 30 ns;
    INT_B <= 100, 15 after 5 ns, -3 after 15 ns, 9 after 25 ns;

    SIGN_A <= to_signed(INT_A, 8);
    SIGN_B <= to_signed(INT_B, 8);

end ARCHI1;
