----------------------------------------------
-- INTEGER adder
----------------------------------------------
entity ADDER1 is
    port (
        A : in  integer;
        B : in  integer;
        S : out integer
        );
end ADDER1;

architecture Behavioral of ADDER1 is
begin

    S <= A + B;

end Behavioral;
