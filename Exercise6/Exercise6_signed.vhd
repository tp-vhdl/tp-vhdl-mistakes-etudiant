----------------------------------------------
-- SIGNED adder2
----------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity ADDER2 is
    port (
        A : in  signed(7 downto 0);
        B : in  signed(7 downto 0);
        S : out signed(7 downto 0)
        );
end ADDER2;

architecture Behavioral of ADDER2 is
begin

    S <= A + B;

end Behavioral;
